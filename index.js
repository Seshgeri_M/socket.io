var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var bodyparser = require('body-parser');

var port = process.env.PORT || 3000;

app.use(bodyparser.json());

const mongoose = require('mongoose');

mongoose.Promise = require("bluebird");

const url = "mongodb://localhost:27017/socketio";

const connect = mongoose.connect(url, { useNewUrlParser: true });

var model = mongoose.model("chats", {
  senderName: String,
  message: String
});


app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
  socket.on('chat message', function (msg, person) {
    var res = person.concat(" ", ":", " ", msg);
    console.log('---', res);
    io.emit('chat message', res);
    connect.then(db => {
      console.log("connected correctly to the server");
      let chatMessage = new model({ senderName: person, message: msg });
      console.log("connected", chatMessage);
      chatMessage.save();
    });
  });
});

http.listen(port, function () {
  console.log('listening on *:' + port);
});
